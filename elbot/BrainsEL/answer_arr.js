var data = {'answer':[$ANS$],
	'time_up': 0,
}
correct = 0;
ret2="";
var form = $('#question_form').get(0);
        var parameter = {
            'content' : form.content.value,
            'course' : form.course.value,
            'subject' : form.subject.value,
            'step' : form.step.value,
            'class' : form.class.value,
            'phase' : form.phase.value,
            'reference_step' : form.reference_step.value,
            'question_order' : form.question_order.value,
            'question_result_id' : form.question_result_id.value,
            'is_answered' : form.is_answered.value,
            'hash' : form.hash.value,
            'time' : form.time.value,
            'data' : data,
        }
        
        self.ajax_object = $.ajax({
            url: '/api/learning/check_answer.php',
            type: 'POST',
            data: parameter,
            dataType: 'json'
        })
        .done(function(data){
            if(data['result'] == 'success') {
                
                //パラメータをセットしなおす
                var form = $('#question_form').get(0);
                form.content.value = data['question_form']['content'];
                form.course.value = data['question_form']['course'];
                form.subject.value = data['question_form']['subject'];
                form.step.value = data['question_form']['step'];
                form.class.value = data['question_form']['class'];
                form.phase.value = data['question_form']['phase'];
                form.reference_step.value = data['question_form']['reference_step'];
                form.question_order.value = data['question_form']['question_order'];
                form.question_result_id.value = data['question_form']['question_result_id'];
                form.is_answered.value = data['question_form']['is_answered'];
                form.hash.value = data['question_form']['hash'];
                form.time.value = data['question_form']['time'];
			}
			correct = data['question_data']['result'];
			ret2 = data['question_data']['complete_text'];});