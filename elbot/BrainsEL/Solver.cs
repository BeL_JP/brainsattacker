﻿using MySql.Data.MySqlClient;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using elbot;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace elbot.BrainsEL
{
    public class Solver
    {
        public event EventHandler Finished;
        public readonly bool mySQLConnected;
        public string reportMessage = "";

        private static MySqlConnection mySQLConnection;
        private string jscript;
        private string jscriptArr;
        private const string url = "http://www.brains-el.jp";
        private const string tablename = "main";
        private List<string> ContentUrlList;
        private string id;
        private string password;
        private bool getAnswerMode = false;
        private int speedMin = 5000;
        private int speedMax = 20000;
        private bool login = false;
        private bool tryLogin = false;
        private System.Collections.ObjectModel.ReadOnlyCollection<Cookie> cks;
        private Questions questions;

        public Solver()
        {
            mySQLConnected = true;
            try
            {
                mySQLConnection = new MySqlConnection("server=bel.kdns.info;user id=globaldb;password=bRO02_RRR7;charset=utf8;database=el_db;Connect Timeout=60;");
                mySQLConnection.Open();
            }
            catch
            {
                mySQLConnection = null;
                mySQLConnected = false;
            }
            LoadScript();
        }

        public void Initialize(string _id, string _password, bool _getAnswerMode, int _speedMin, int _speedMax)
        {
            id = _id;
            password = _password;
            getAnswerMode = _getAnswerMode;
            speedMin = _speedMin;
            speedMax = _speedMax;
            questions = new Questions(mySQLConnection, tablename);
        }

        public void Begin(List<string> _contentUrlList)
        {
            ContentUrlList = _contentUrlList;
            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(BotThread));
            t.Start();
        }

        public void Dispose()
        {
            mySQLConnection?.Close();
        }

        private void LoadScript()
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.StreamReader sr = new System.IO.StreamReader(
                asm.GetManifestResourceStream("elbot.BrainsEL.answer.js"), System.Text.Encoding.GetEncoding("shift-jis"));
            jscript = sr.ReadToEnd();
            sr.Close();
            System.IO.StreamReader sr2 = new System.IO.StreamReader(
                asm.GetManifestResourceStream("elbot.BrainsEL.answer_arr.js"), System.Text.Encoding.GetEncoding("shift-jis"));
            jscriptArr = sr2.ReadToEnd();
            sr2.Close();
        }

        private ChromeDriver Login()
        {
            while (tryLogin)
            {
                System.Threading.Thread.Sleep(500);
            }
            tryLogin = true;
            var opt = new ChromeOptions();
            opt.AddArgument("--mute-audio");
            ChromeDriver drv = new ChromeDriver(opt);
            if (!login)
            {
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                drv.Navigate().GoToUrl(url);
                drv.FindElement(By.XPath("//input[@data-name='login_id']")).SendKeys(id);
                drv.FindElement(By.XPath("//input[@data-name='password']")).SendKeys(password);
                drv.FindElement(By.XPath("//button[@type='submit']")).Submit();
                cks = drv.Manage().Cookies.AllCookies;
                login = true;
            }
            else
            {
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                drv.Navigate().GoToUrl(url);
                foreach (var ck in cks)
                    drv.Manage().Cookies.AddCookie(ck);
                drv.Navigate().Refresh();
            }
            tryLogin = false;
            return drv;
        }

        private void BotThread()
        {
            foreach (var contentUrl in ContentUrlList)
            {
                ChromeDriver drv = Login();

                //ダッシュボードへ移動
                while (drv.Url != "https://www.brains-el.jp/dashboard/")
                    System.Threading.Thread.Sleep(100);
                bool selected;
                int answeredCounter = 0;
                while (true)
                {
                    selected = false;
                    //コンテンツトップに移動
                    drv.Navigate().GoToUrl(contentUrl);
                    try
                    {
                        var subContents = drv.FindElements(By.CssSelector("a.list-group-item.clearfix"));
                        if (subContents == null || subContents.Count == 0)
                            break;
                        foreach (var subContent in subContents)
                        {

                            if (getAnswerMode)
                            {
                                //解答済みコンテンツへ移動
                                var elements = subContent.FindElements(By.CssSelector("span.small"));
                                if (elements.Count > answeredCounter && elements[answeredCounter].Text != "0%")
                                {
                                    elements[answeredCounter++].Click();
                                    selected = true;
                                    break;
                                }
                            }
                            else
                            {
                                //最上部の未完了サブコンテンツへ移動
                                if (subContent.FindElement(By.CssSelector("span.small")).Text != "100%")
                                {
                                    subContent.Click();
                                    selected = true;
                                    break;
                                }
                            }
                        }
                    }
                    catch
                    {
                        break;
                    }
                    if (!selected)
                        break;
                    while (drv.Url == contentUrl)
                        System.Threading.Thread.Sleep(100);
                    SolveSubContents(drv, drv.Url);
                }
                drv.Close();
                drv.Quit();
                Finished?.Invoke(this, EventArgs.Empty);
            }
        }

        private void SolveSubContents(IWebDriver drv, string subContentsUrl)
        {
            int answeredCounter = 0;
            while (true)
            {
                while (drv.Url != subContentsUrl)
                    System.Threading.Thread.Sleep(100);
                try
                {
                    //最上部の未回答問題を選択
                    drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 3);
                    var elements = getAnswerMode ? drv.FindElements(By.CssSelector("a.class_button.btn.btn-info"))
                        : drv.FindElements(By.CssSelector("a.class_button.btn.btn-warning"));
                    if (elements.Count != 0)
                    {
                        if (getAnswerMode)
                        {
                            while (elements.Count > answeredCounter)
                            {
                                if (elements[answeredCounter].Text == "" || elements[answeredCounter].Text.Contains("間違い"))
                                    answeredCounter++;
                                else
                                    break;
                            }
                            if (elements.Count <= answeredCounter)
                                break;
                            elements[answeredCounter].Click();
                            answeredCounter++;
                        }
                        else
                            elements[0].Click();
                    }
                    else
                    {
                        drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                        break;
                    }
                }
                catch
                {
                    drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                    break;
                }
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                while (drv.Url != "https://www.brains-el.jp/learning/question.html")
                    System.Threading.Thread.Sleep(100);

                SolveQuestions(drv);
                drv.Navigate().GoToUrl(subContentsUrl);
            }
        }

        private void SolveQuestions(IWebDriver drv)
        {
            Questions questions = new Questions(mySQLConnection, tablename);
            int answeredCounter = 0;
            int answerSelect = 0;
            int answerLocked = 2;
            bool endFlag = false;
            bool forceArrMode = false;
            List<Question> ignoreQuestions = new List<Question>();
            while (!endFlag && drv.Url == "https://www.brains-el.jp/learning/question.html")
            {
                System.Random r = new System.Random();
                //終了判定
                if (getAnswerMode ^ drv.FindElement(By.CssSelector("div.panel-heading.clearfix")).Text.Contains("復習"))
                {
                    //break;
                }
                //解答待機
                if (getAnswerMode)
                {
                    string rightCnt = drv.FindElement(By.CssSelector("span.right_count")).Text;
                    string wrongCnt = drv.FindElement(By.CssSelector("span.wrong_count")).Text;
                    if (drv.FindElement(By.CssSelector("li.question_count")).Text.Split('/')[1] == "1")
                        endFlag = true;
                    if (int.Parse(rightCnt) + int.Parse(wrongCnt) < answeredCounter)
                    {
                        answeredCounter = 0;
                        break;
                    }

                    answeredCounter = int.Parse(rightCnt) + int.Parse(wrongCnt);
                    System.Threading.Thread.Sleep(1000);
                }
                else
                    System.Threading.Thread.Sleep(r.Next(speedMin, speedMax));
                if (getAnswerMode ^ drv.FindElement(By.CssSelector("div.panel-heading.clearfix")).Text.Contains("復習"))
                {
                    //break;
                }

                //問題識別用のテキストをセット
                string questionText = "";

                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 3);
                try
                {
                    questionText += drv.FindElement(By.Id("audio")).GetAttribute("src");
                }
                catch { }
                if (questionText == "")
                {
                    try
                    {
                        questionText += drv.FindElement(By.CssSelector("p.hint_japanese")).Text;
                    }
                    catch { }
                    try
                    {
                        questionText += drv.FindElement(By.CssSelector("p.blanked_text")).Text;
                    }
                    catch { }
                }

                //データベースから問題識別
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                var matchedQuestions = questions.GetQuestions(questionText);


                //解答処理
                //未知の問題
                if (matchedQuestions.Count == 0)
                {
                    Question voidQuestion = new Question(questionText, false, new List<string>());
                    Question newQuestion = null;
                    Answer(drv, voidQuestion, out newQuestion, forceArrMode);
                    questions.AddQuestion(newQuestion);
                }
                //既知の問題
                else
                {
                    bool retry = true;
                    int limit = matchedQuestions.Count;
                    if (answerLocked == 0)
                    {
                        answerLocked = 2;
                        answerSelect = (answerSelect + 1) % matchedQuestions.Count;
                        while (retry)
                        {
                            retry = false;
                            foreach (var ignoreQ in ignoreQuestions)
                            {
                                if (Equals(ignoreQ, matchedQuestions[answerSelect % matchedQuestions.Count]))
                                {
                                    answerSelect = (answerSelect + 1) % matchedQuestions.Count;
                                    retry = true;
                                    break;
                                }
                            }
                            if (!retry)
                                break;
                            limit--;
                            if (limit == 0)
                                break;
                        }
                    }
                    Question newQuestion = null;
                    if (!Answer(drv, matchedQuestions[answerSelect % matchedQuestions.Count], out newQuestion, forceArrMode))
                    {
                        if (limit != 0)
                            ignoreQuestions.Add(matchedQuestions[answerSelect % matchedQuestions.Count]);
                        answerLocked--;
                    }
                    else
                    {
                        answerLocked = 2;
                    }
                    ignoreQuestions.RemoveAll(q => Equals(q, newQuestion));
                    questions.AddQuestion(newQuestion);
                }
                //次の問題への遷移
                drv.Navigate().Refresh();
            }
        }

        private string GetSendAnswerString(Question question)
        {
            string sendAnswer = "";
            if (question.IsArr)
            {
                for (int i = 0; i < question.Answers.Count; i++)
                {
                    sendAnswer += "\"";
                    sendAnswer += (question.Answers[i] != null ? question.Answers[i] : "").Replace("\\", "\\\\").Replace("\"", "\\\"");
                    sendAnswer += "\"";
                    if (i + 1 < question.Answers.Count)
                    {
                        sendAnswer += ",";
                    }
                }
            }
            else
            {
                sendAnswer = "\"" + (question.Answers.Count != 0 ? question.Answers[0] : "").Replace("\\", "\\\\").Replace("\"", "\\\"") + "\"";
            }
            return sendAnswer;
        }

        private bool Answer(IWebDriver drv, Question question, out Question newQuestion, bool forceArrMode)
        {
            reportMessage = "question.Text = " +question.Text + "\n";
            reportMessage += "question.IsArr = " +question.IsArr.ToString() + "\n";
            foreach (var answer in question.Answers)
                reportMessage += "question.answer[] = " + answer + "\n";
            reportMessage += "\n\n\n" + drv.FindElement(By.XPath("//*")).GetAttribute("outerHTML");
            string sendAnswer = GetSendAnswerString(question);
            List<string> newAnswers = new List<string>();
            bool isDict = false;
            //ディクテーション
            if ((isDict = drv.FindElement(By.CssSelector("span.course_title")).Text.Contains("ディクテーション")) || drv.FindElement(By.CssSelector("div.panel-heading.clearfix")).Text.Contains("並べ替え"))
            {
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 5);
                while (true)
                {

                    if (isDict)
                    {
                        int elementIndex = 0;
                        int id = 0;
                        var elements = drv.FindElements(By.CssSelector("input.blank"));
                        if (elements.Count == 0)
                            break;
                        while (elements.Count != elementIndex)
                        {
                            if ((elements[elementIndex].GetAttribute("type") == "hidden"
                            || elements[elementIndex].GetAttribute("class") == "blank answered wrong"
                            || elements[elementIndex].GetAttribute("class") == "blank answered correct")
                            || elements[elementIndex].GetAttribute("value") != "")
                               elementIndex++;
                            else
                                break;
                        }
                        if (elementIndex == elements.Count)
                            break;
                        id = int.Parse(elements[elementIndex].GetAttribute("data-blank_id")) - 1;
                        try
                        {
                            elements[elementIndex].SendKeys(question.Answers.Count <= id ? "hoge" : question.Answers[id]);
                        }
                        catch { }
                    }
                    else
                    {
                        System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> elements = null;
                        try
                        {
                            elements = drv.FindElements(By.CssSelector("a.each_choice.ui-draggable.ui-draggable-handle"));
                        }
                        catch { }
                        if (elements == null)
                            break;
                        int count = drv.FindElements(By.CssSelector("span.blank.ui-droppable.ui-draggable.ui-draggable-handle")).Count;
                        for (int id = 0; id < count; id++)
                        {
                            bool selected = false;
                            foreach (var e in elements)
                            {
                                if (e.GetAttribute("class") != "each_choice ui-draggable ui-draggable-handle disabled ui-draggable-disabled"
                                    && (question.Answers.Count <= id || e.GetAttribute("data-answer") == question.Answers[id]))
                                {
                                    selected = true;
                                    e.Click();
                                    break;
                                }
                            }
                            if (!selected)
                                elements[0].Click();
                            System.Threading.Thread.Sleep(200);
                        }
                        System.Threading.Thread.Sleep(200);
                        try
                        {
                            drv.FindElement(By.CssSelector("button.answer.btn.btn-warning")).Click();
                        }
                        catch { }
                        break;
                    }

                    System.Threading.Thread.Sleep(200);
                    try
                    {
                        drv.FindElement(By.CssSelector("button.answer.btn.btn-warning")).Click();
                    }
                    catch { }
                    System.Threading.Thread.Sleep(500);
                }
                var newAns = drv.FindElements(By.CssSelector("span.marked"));
                foreach (var ans in newAns)
                {
                    newAnswers.Add(ans.Text);
                }
                newQuestion = new Question(question.Text, true, newAnswers);

                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 3);
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> goodElements;
                try
                {
                    goodElements = drv.FindElements(By.XPath("//img[@src='/resource/images/good_stamp.png']"));
                }
                catch
                {
                    drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                    return false;
                }
                drv.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
                return goodElements.Count != 0;
            }
            else
            {
                if (!(question.IsArr) && !forceArrMode)
                {
                    //解答送信
                    drv.ExecuteJavaScript(jscript.Replace("$ANS$", sendAnswer));
                    while (drv.ExecuteJavaScript<string>("return ret;") == "")
                        System.Threading.Thread.Sleep(200);
                    if (drv.ExecuteJavaScript<string>("return ret;") != null)
                    {
                        newAnswers.Add(drv.ExecuteJavaScript<string>("return ret;"));
                        newQuestion = new Question(question.Text, false, newAnswers);
                        return drv.ExecuteJavaScript<Int64>("return correct;") == 1;
                    }
                    forceArrMode = true;
                    newQuestion = null;
                    return false;
                }

                //解答送信（配列）
                drv.ExecuteJavaScript(jscriptArr.Replace("$ANS$", sendAnswer));
                int retryCount = 5;
                while (drv.ExecuteJavaScript<string>("return ret2;") == "" && retryCount > 0)
                {
                    System.Threading.Thread.Sleep(200);
                    retryCount--;
                }
                if (retryCount == 0)
                {
                    forceArrMode = false;
                    newQuestion = null;
                    return false;
                }
                //正答取得
                string text = drv.ExecuteJavaScript<string>("return ret2;");
                newAnswers.AddRange(ParseCompleteText(text));
            }

            newQuestion = new Question(question.Text, true, newAnswers);
            return drv.ExecuteJavaScript<Int64>("return correct;") == 1;
        }

        private List<string> ParseCompleteText(string text)
        {
            List<string> ret = new List<string>();
            int i = 0;
            string buf = "";
            bool flag = false;
            for (int j = 0; j < text.Length; j++)
            {
                if (!flag && text[j] == '(')
                {
                    flag = true;
                }
                else if (flag)
                {
                    if (text[j] == ')')
                    {
                        flag = false;
                        ret.Add(buf);
                        buf = "";
                        i++;
                    }
                    else
                    {
                        buf += text[j];
                    }
                }
            }
            return ret;
        }
    }
}