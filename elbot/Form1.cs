﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace elbot
{
    public partial class Form1 : Form
    {
        private BrainsEL.Solver belsolver = null;
        System.Threading.Thread t;
        private bool initialized = false;
        private int tCount = 3;

        public Form1()
        {
            t = new System.Threading.Thread(new System.Threading.ThreadStart(StartUPThread));
            InitializeComponent();
            Text += Program.Revision;
            textBox1.Text = "br010588";
            textBox2.Text = "0wfzs7hh";
        }
        
        private void Form1_Shown(object sender, EventArgs e)
        {
            t.Start();
        }

        private void StartUPThread()
        {
            if (Program.CheckUpdate())
            {
                label5.Visible = true;
                Program.Update();
                return;
            }
            Invoke((MethodInvoker)delegate
            {
                label4.Text = "DB接続中...";
            });
            belsolver = new BrainsEL.Solver();
            belsolver.Finished += Belsolver_Finished;
            if (!belsolver.mySQLConnected)
            {
                Invoke((MethodInvoker)delegate
                {
                    this.Text += " [DB未接続]";
                    labelLog.Text = "データベースに接続できませんでした．";
                });
            }
            Invoke((MethodInvoker)delegate
            {
                panel1.Visible = false;
            });
            timer1.Start();
        }

        private void Belsolver_Finished(object sender, EventArgs e)
        {
            tCount--;
            if(tCount == 0)
            {
                Invoke((MethodInvoker)delegate
                {
                    button1.Text = "完了";
                    labelLog.Text = "解答終了しました";
                });
                belsolver.Dispose();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("ID，パスワードを入力してください．", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            button1.Text = "解答中";
            button1.Enabled = false;
            buttonHoukoku.Visible = true;
            BeginBot("https://www.brains-el.jp/learning/?content=1", "https://www.brains-el.jp/learning/?content=3", "https://www.brains-el.jp/learning/?content=5");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            minbox.Visible = true;
            maxbox.Visible = true;
            maxminLabel.Visible = true;
        }

        private void BeginBot(params string[] _contentUrl)
        {
            if (!initialized)
            {
                initialized = true;
                belsolver.Initialize(textBox1.Text, textBox2.Text, checkBox1.Checked, int.Parse(minbox.Text), int.Parse(maxbox.Text));
            }
            belsolver.Begin(new List<string>(_contentUrl));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Program.CheckUpdate())
            {
                labelLog.Text = "アップデートが見つかりました";
            }
        }

        private void ButtonHoukoku_Click(object sender, EventArgs e)
        {
            string message = "Date : " + DateTime.Now.ToString() + "`\n";
            message += "User : " + textBox1.Text +"\n";
            message += "Pass : " + textBox2.Text +"\n";
            message += belsolver.reportMessage;
            string mailto = "mailto:3days.bouzu+bugReport@gmail.com?subject=bugReport&body=" + Uri.EscapeUriString(message);
            Process.Start(mailto);
        }
    }
}