﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace elbot
{
    public class Question
    {
        public string Text;
        public bool IsArr;
        public List<string> Answers;
        public Question(string _text, bool _isarr, List<string> _answers)
        {
            Text = _text;
            IsArr = _isarr;
            Answers = _answers;
        }

        public override bool Equals(object obj)
        {
            var @class = obj as Question;
            if (@class == null || Text != @class.Text || IsArr != @class.IsArr || @class.Answers.Count != Answers.Count)
                return false;
            for (int i = 0; i < Answers.Count; i++)
            {
                if (@class.Answers[i] != Answers[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            return EqualityComparer<string>.Default.GetHashCode(Text + IsArr.ToString() + String.Join("", Answers));
        }
    }

    /// <summary>
    /// データベースはUTF-8で作成してください！
    /// </summary>
    public class Questions
    {
        private bool localMode = false;
        private List<Question> questionList;
        private MySqlDataAdapter da;
        private MySqlConnection conn;
        private string tablename;
        private int counter = 0;
        public Questions(MySqlConnection mySqlConnection, string _tablename)
        {
            questionList = new List<Question>();
            if (mySqlConnection == null)
            {
                localMode = true;
                return;
            }
            conn = mySqlConnection;
            tablename = _tablename;
            //データベースから解答取得
            // SQL文と接続情報を指定し、データアダプタを作成
            da = new MySqlDataAdapter("select question, answer, isarr from " + tablename, conn);
            // データ取得
            SetDataTable();
            //_FixDataTable();
        }

        public List<Question> GetQuestions(string questionText)
        {
            if (!localMode)
                SetDataTable();
            return questionList.FindAll(q => q.Text == questionText);
        }

        public void AddQuestion(Question newQuestion)
        {
            if (newQuestion == null)
                return;
            foreach (var question in questionList)
            {
                if (Equals(question, newQuestion))
                    return;
            }
            questionList.Add(newQuestion);
            if (!localMode)
            {
                lock (conn)
                {
                    // コマンドを作成
                    MySqlCommand cmd =
                        new MySqlCommand("insert into " + tablename + " ( question, answer, isarr ) values ( @question, @answer, @isarr )", conn);
                    // パラメータ設定
                    cmd.Parameters.Add(
                        new MySqlParameter("question", UTF8Encoding.UTF8.GetBytes(newQuestion.Text)));
                    cmd.Parameters.Add(
                        new MySqlParameter("answer", UTF8Encoding.UTF8.GetBytes(ArrayToString(newQuestion.Answers.ToArray()))));
                    cmd.Parameters.Add(
                        new MySqlParameter("isarr", newQuestion.IsArr ? 1 : 0));

                    // 実行
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// 重複データを除いたものをcopyTableNameテーブルに書き込む
        /// </summary>
        /// <param name="copyTableName"></param>
        private void _FixDataTable(string copyTableName)
        {
            SetDataTable();
            var copyQuestionList = new List<Question>(questionList);
            foreach (var q in questionList)
            {
                if (copyQuestionList.FindAll(q1 => Equals(q1, q)).Count > 100)
                {
                    copyQuestionList.RemoveAll(q1 => Equals(q1, q));
                }
                else if (copyQuestionList.FindAll(q1 => Equals(q1, q)).Count > 1)
                {
                    copyQuestionList.RemoveAll(q1 => Equals(q1, q));
                    copyQuestionList.Add(q);
                }
            }
            int i = 0;
            foreach (var q in copyQuestionList)
            {
                Console.WriteLine(i++ + " of " + copyQuestionList.Count + " (" + 100 * i / copyQuestionList.Count + "%)");
                // コマンドを作成
                MySqlCommand cmd =
                    new MySqlCommand("insert into " + copyTableName + " ( question, answer, isarr ) values ( @question, @answer, @isarr )", conn);
                // パラメータ設定
                cmd.Parameters.Add(
                    new MySqlParameter("question", UTF8Encoding.UTF8.GetBytes(q.Text)));
                cmd.Parameters.Add(
                    new MySqlParameter("answer", UTF8Encoding.UTF8.GetBytes(ArrayToString(q.Answers.ToArray()))));
                cmd.Parameters.Add(
                    new MySqlParameter("isarr", q.IsArr ? 1 : 0));

                // 実行
                cmd.ExecuteNonQuery();
            }
            SetDataTable();
        }

        private void SetDataTable()
        {
            if(counter == 0 && questionList != null)
            {
                lock (questionList)
                {
                    questionList = new List<Question>();
                    // データを格納するテーブル作成
                    DataTable dt = new DataTable();
                    lock (conn)
                        lock (da)
                            da.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                    {
                        questionList.Add(new Question((string)row["question"],
                            (int)row["isarr"] == 1 ? true : false, new List<string>(StringToArray((string)row["answer"]))));
                    }
                }
            }
            counter = (counter + 1) % 5;
        }

        private static string ArrayToString(string[] strs)
        {
            string ret = "";
            foreach (var str in strs)
            {
                var newstr = str;
                newstr = newstr.Replace("\\", "\\\\").Replace("\"", "\\\"");
                ret += "\"" + newstr + "\",";
            }
            ret.TrimEnd(',');
            return ret;
        }

        private static List<string> StringToArray(string str)
        {
            List<string> ret = new List<string>();
            bool flag = false;
            bool sc = false;
            string buf = "";
            foreach (var c in str)
            {
                if (c == '\\' && !sc)
                {
                    sc = true;
                }
                else if (sc)
                {
                    buf += c;
                    sc = false;
                }
                else
                {
                    if (c == '"')
                    {
                        if (!flag)
                        {
                            buf = "";
                        }
                        flag = !flag;
                    }
                    else if (flag)
                    {
                        buf += c;
                    }
                    else if (c == ',')
                        ret.Add(buf);
                }
            }
            return ret;
        }
    }
}