﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;

namespace elbot
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        public static int Revision = 31;
        private static int latestRevision = Revision;
        static string url = "https://bitbucket.org/";
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new Form1());
        }

        public static bool CheckUpdate()
        {
            //WebClientを作成
            System.Net.WebClient wc = new System.Net.WebClient();
            //文字コードを指定
            wc.Encoding = System.Text.Encoding.UTF8;
            //データを文字列としてダウンロードする
            string source = wc.DownloadString(url + "BeL_JP/brainsattacker/downloads/?tab=tags");
            //後始末
            System.Text.RegularExpressions.Regex r =
                new System.Text.RegularExpressions.Regex("/BeL_JP/brainsattacker/get/r(.*?)[.]zip");
            System.Text.RegularExpressions.MatchCollection mc = r.Matches(source);
            foreach (System.Text.RegularExpressions.Match m in mc)
            {
                int val = 0;
                if (int.TryParse(m.Groups[1].Value, out val) && latestRevision < val)
                    latestRevision = val;
            }
            if (File.Exists("tmp"))
            {
                System.Threading.Thread.Sleep(1000);
                try
                {
                    File.Delete("tmp");
                }
                catch
                {

                }
            }
            if (Revision >= latestRevision)
            {
                wc.Dispose();
                return false;
            }
            wc.Dispose();
            return true;
        }

        public static void Update()
        {
            //WebClientを作成
            System.Net.WebClient wc = new System.Net.WebClient();
            //文字コードを指定
            wc.Encoding = System.Text.Encoding.UTF8;
            if (File.Exists("tmp"))
            {
                System.Threading.Thread.Sleep(1000);
                try
                {
                    File.Delete("tmp");
                }
                catch
                {

                }
            }
            if (Revision < latestRevision)
            {
                if (Directory.Exists(@"tmpex\"))
                {
                    Directory.Delete("tmpex/", true);
                }
                wc.DownloadFile(url + "BeL_JP/brainsattacker/get/r" + latestRevision + ".zip", "tmp");
                ZipFile.ExtractToDirectory("tmp", @"tmpex\");
                File.Delete("tmp");
                File.Move("elbot.exe", "tmp");
                File.Copy(Directory.GetDirectories(@"tmpex\")[0] + @"\elbot\bin\Release\elbot.exe", "elbot.exe");
                Directory.Delete(@"tmpex\", true);
            }
            else
            {
                wc.Dispose();
                return;
            }
            wc.Dispose();
            System.Diagnostics.Process.Start("elbot.exe");
            Application.Exit();
            return;
        }

        // JavaScriptを実行（戻り値なし）
        public static void ExecuteJavaScript(this IWebDriver driver, string script)
        {
            if (driver is IJavaScriptExecutor)
                ((IJavaScriptExecutor)driver).ExecuteScript(script);
            else
                throw new WebDriverException();
        }

        // JavaScriptを実行（戻り値あり）
        public static T ExecuteJavaScript<T>(this IWebDriver driver, string script)
        {
            if (driver is IJavaScriptExecutor)
                return (T)((IJavaScriptExecutor)driver).ExecuteScript(script);
            else
                throw new WebDriverException();
        }
    }

}
