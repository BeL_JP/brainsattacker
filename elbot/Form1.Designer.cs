﻿namespace elbot
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.maxminLabel = new System.Windows.Forms.Label();
            this.maxbox = new System.Windows.Forms.TextBox();
            this.minbox = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.labelLog = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonHoukoku = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(202, 19);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(70, 72);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(202, 19);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "ログインID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "パスワード";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(96, 228);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 35);
            this.button1.TabIndex = 4;
            this.button1.Text = "開始";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // maxminLabel
            // 
            this.maxminLabel.AutoSize = true;
            this.maxminLabel.Location = new System.Drawing.Point(104, 139);
            this.maxminLabel.Name = "maxminLabel";
            this.maxminLabel.Size = new System.Drawing.Size(85, 12);
            this.maxminLabel.TabIndex = 5;
            this.maxminLabel.Text = "min - max [ms]";
            this.maxminLabel.Visible = false;
            // 
            // maxbox
            // 
            this.maxbox.Location = new System.Drawing.Point(148, 117);
            this.maxbox.Name = "maxbox";
            this.maxbox.Size = new System.Drawing.Size(124, 19);
            this.maxbox.TabIndex = 6;
            this.maxbox.Text = "6000";
            this.maxbox.Visible = false;
            // 
            // minbox
            // 
            this.minbox.Location = new System.Drawing.Point(14, 117);
            this.minbox.Name = "minbox";
            this.minbox.Size = new System.Drawing.Size(128, 19);
            this.minbox.TabIndex = 7;
            this.minbox.Text = "4000";
            this.minbox.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(96, 154);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(98, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "急ぐ！";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 331);
            this.panel1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(3, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(284, 51);
            this.label5.TabIndex = 14;
            this.label5.Text = "アップデートが見つかりました";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("游ゴシック", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(0, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(284, 27);
            this.label4.TabIndex = 0;
            this.label4.Text = "アップデート確認中...";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(96, 183);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(100, 16);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "解答取得モード";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // labelLog
            // 
            this.labelLog.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelLog.ForeColor = System.Drawing.Color.Lime;
            this.labelLog.Location = new System.Drawing.Point(12, 295);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(260, 24);
            this.labelLog.TabIndex = 13;
            this.labelLog.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonHoukoku
            // 
            this.buttonHoukoku.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonHoukoku.Location = new System.Drawing.Point(23, 269);
            this.buttonHoukoku.Name = "buttonHoukoku";
            this.buttonHoukoku.Size = new System.Drawing.Size(238, 23);
            this.buttonHoukoku.TabIndex = 15;
            this.buttonHoukoku.Text = "解答が止まった場合，クリックして報告";
            this.buttonHoukoku.UseVisualStyleBackColor = true;
            this.buttonHoukoku.Visible = false;
            this.buttonHoukoku.Click += new System.EventHandler(this.ButtonHoukoku_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 328);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.minbox);
            this.Controls.Add(this.maxbox);
            this.Controls.Add(this.maxminLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.labelLog);
            this.Controls.Add(this.buttonHoukoku);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Brains-EL Bot r";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label maxminLabel;
        private System.Windows.Forms.TextBox maxbox;
        private System.Windows.Forms.TextBox minbox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonHoukoku;
    }
}

